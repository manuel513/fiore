import {
  GET_PREGUNTAS,
  GET_CODIGOS,
  GET_RESULTADOS,
  GET_CONFIG,
  LOGIN_ADMIN,
  LOGOUT_ADMIN,
  POST_PREGUNTA,
  POST_CODIGO,
  POST_RESULTADO,
  SET_CODIGO,
  ADD_RESULTADO,
  PATCH_PREGUNTA,
  SWAP_PREGUNTA,
  DELETE_PREGUNTA,
  SET,
  CLEAR,
  CLEAR_ERROR,
  SET_ERROR
} from '../constants';

const initialState = {
  preguntas: [],
  resultados: [],
  codigos: [],
  codigo: null,
  error: null,
  token: null,
  config: {
    titulo: 'Encuesta',
    activa: true
  },
  flag: false
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PREGUNTAS:
      action.payload.sort((a, b) => a.indice - b.indice);
      return { ...state, preguntas: action.payload };
    case GET_CODIGOS:
      return { ...state, codigos: action.payload };
    case GET_RESULTADOS:
      return { ...state, resultados: action.payload };
    case GET_CONFIG:
      return { ...state, config: action.payload };
    case LOGIN_ADMIN:
      return { ...state, token: action.payload };
    case LOGOUT_ADMIN:
      return { ...state, token: null };
    case POST_PREGUNTA:
      return { ...state, preguntas: [...state.preguntas, action.payload] };
    case POST_CODIGO:
      return {
        ...state,
        codigos: state.codigos.concat(action.payload.codigos)
      };
    case POST_RESULTADO:
      return state;
    case SET_CODIGO:
      return {...state, codigo: action.payload}
    case PATCH_PREGUNTA:
      let preguntas_patch = state.preguntas;
      let indice_patch = preguntas_patch.findIndex(pregunta => {
        return pregunta.indice === action.payload.indice;
      });

      if (indice_patch !== undefined) {
        preguntas_patch[indice_patch] = action.payload;
      }

      return { ...state, preguntas: [...preguntas_patch] };
    case SWAP_PREGUNTA:
      return { ...state, flag: true };
    case DELETE_PREGUNTA:
      let preguntas_delete = state.preguntas.filter(pregunta => {
        return pregunta.id !== action.payload;
      });
      return { ...state, preguntas: preguntas_delete };
    case SET_ERROR:
      if (action.payload.statusCode === 401 && state.token) {
        action.payload['message'] = 'Su sesion ha expirado';
      }
      return {
        ...state,
        error: action.payload,
        token: action.payload.logout ? null : state.token
      };
    case CLEAR_ERROR:
      return { ...state, error: null };
    case ADD_RESULTADO:
      return { ...state, resultados: [...state.resultados, action.payload] };
    case SET:
      return { ...state, flag: action.payload };
    case CLEAR:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};

export default rootReducer;
