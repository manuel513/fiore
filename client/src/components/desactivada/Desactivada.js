import React from 'react'
import { Container, Row, Col, Jumbotron, Button } from 'reactstrap'
import { Link } from 'react-router-dom'
import { IoIosConstruct } from 'react-icons/io'

export default (props) => {
  return (
    <Container>
      <Row>
        <Col>
          <Jumbotron>
            <div className='text-center'>
              <IoIosConstruct className='Big-logo'/>
            </div>
            <div className='mt-4 text-center'>
              <span className='text-weight-bold display-3'>Estamos construyendo una increible encuesta</span>
              <p className='h2'>Por favor te pedimos un poco de paciencia</p>
              <Button tag={Link} to='/encuesta' color='success'>Reintentar</Button>
            </div>
          </Jumbotron>
        </Col>
      </Row>
    </Container>
  )
}
