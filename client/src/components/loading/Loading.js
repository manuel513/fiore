import React from 'react'
import { FaSyncAlt } from 'react-icons/fa';

export default (props) => {
  return (
    <div className='App-header'>
      <FaSyncAlt className='App-logo' />
    </div>
  )
}
