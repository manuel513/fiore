import React, { Component } from 'react';
import { Container, Row, Col, Button, Form, FormGroup, FormFeedback, Label, Input, Alert } from 'reactstrap'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { loginAdmin, clearError } from '../../actions'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      email: null,
      invalido: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.toggle = this.toggle.bind(this)
    this.enter = this.enter.bind(this)
    this.submit = this.submit.bind(this)
  }

  componentWillUnmount() {
    this.props.clearError()
  }

  handleChange(e) {
    const {name, value} = e.target
    let toState = {invalido: false}
    toState[name] = value
    this.setState(toState)
  }

  toggle() {
    this.props.clearError()
  }

  enter(target) {
    if (target.charCode === 13) {
      this.submit()
    }
  }

  submit() {
    // Honeypot https://solutionfactor.net/blog/2014/02/01/honeypot-technique-fast-easy-spam-prevention/
    const { username, password, email} = this.state
    if (!email && password !== '' && email !== '') {
      this.props.loginAdmin(username, password)
    } else {
      this.setState({invalido: true})
    }
  }

  render() {
    return (
      <Container>
        <Row>
          {this.props.token && (<Redirect to='/admin'/>)}
          <Col xs={12}>
            <p className="text-center text-white font-weight-bold h1 mt-2">Encuestas</p>
          </Col>
        </Row>
        <Row>
          <Col>
            <Alert isOpen={(this.props.error !== null)} toggle={this.toggle} color='danger'>{this.props.error && (this.props.error.message)}</Alert>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={{size: 6, offset: 3}} lg={{size: 4, offset: 4}} className='mt-5'>
            <Form>
              <FormGroup>
                <Label for="exampleEmail" className='text-white font-weight-bold h4'>Usuario</Label>
                <Input type="text" onKeyPress={this.enter} name="username" onChange={this.handleChange} id="exampleEmail" placeholder="Nombre de usuario" valid={(this.state.username !== '')} invalid={(this.state.username === '') && this.state.invalido} />
                <FormFeedback>El campo esta vacio</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="examplePassword" className='text-white font-weight-bold h4'>Contrase&ntilde;a</Label>
                <Input type="password" onKeyPress={this.enter} name="password" onChange={this.handleChange} id="examplePassword" placeholder="Contrase&ntilde;a" valid={(this.state.password !== '')} invalid={(this.state.password === '') && this.state.invalido} />
                <FormFeedback>El campo esta vacio</FormFeedback>
              </FormGroup>
              <FormGroup check>
                <Label className='text-white font-weight-bold' check>
                  <Input type="checkbox" onKeyPress={this.enter} />{' '}
                  Mantener sesion iniciada
                </Label>
              </FormGroup>
              <FormGroup className='d-none'>
                <Label for="email" className='text-white font-weight-bold h4'>Email</Label>
                <Input type="email" name="email" onChange={this.handleChange} id="email"/>
              </FormGroup>
              <Button color='secondary' className='mt-2' onClick={this.submit}>Entrar</Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return { error: state.error, token: state.token }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearError: () => {dispatch(clearError())},
    loginAdmin: (username, password) => {dispatch(loginAdmin(username, password))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
