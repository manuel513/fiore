import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return { error: state.error, token: state.token}
}

export default connect(mapStateToProps, null)(function ({ component: Component, ...rest }) {
  console.log(rest)
  return (
    <Route
      render={props =>
        ((rest.error && rest.error.statusCode === 401) || rest.token === null) ? (
          <Redirect
            to={{
              pathname: `${rest.path}/login`,
              state: { from: props.location }
            }}
          />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
})
