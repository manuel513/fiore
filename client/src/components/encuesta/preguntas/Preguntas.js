import React from 'react'
import Parser from 'html-react-parser';
import { connect } from 'react-redux'
import { Container, Row, Col, Jumbotron, Button } from 'reactstrap'
import { postResultado, setCodigo, clearError } from '../../../actions'
import swal from '@sweetalert/with-react'
import Steps, { Step } from 'rc-steps'
import 'rc-steps/assets/index.css'
import 'rc-steps/assets/iconfont.css'


class Preguntas extends React.PureComponent {

  state = {
    actual: 0,
    respuestas: [],
    enviado: false
  }

  componentDidMount() {
    if (this.props.codigo === '' || this.props.codigo === null) {
      this.props.history.push('/encuesta')
    }

    let respuestas = this.props.preguntas.map(pregunta => {
      return {
         id: pregunta.id,
         tipo: pregunta.tipo,
         respuesta: pregunta.tipo  === 'multiple' ? [] : ''
      }
    })

    this.setState({respuestas})
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.error && this.props.error.statusCode === 422) {
      swal('Error', 'El codigo proporcionado no existe o ya ha sido utilizado', 'error')
      .then(function() {
        this.setState({enviado: false})
        this.props.clearError()
      }.bind(this))
    } else if(this.state.enviado && prevProps.error === null) {
      swal('¡Exito!', 'Su encuesta ha sido enviada', 'success')
      .then(function() {
        this.props.setCodigo(null)
        this.props.history.push('/gracias')
      }.bind(this))
    }
  }

  setRespuestaNoMultiple(e) {
    let indico = e.target.value
    let respuestas = this.state.respuestas
    respuestas[this.state.actual].respuesta = indico
    this.setState({respuestas: respuestas})
  }

  setRespuestaMultiple(e) {
    let respuestas = this.state.respuestas
    let respuesta = respuestas[this.state.actual].respuesta
    if (e.target.checked) {
      if (!respuesta.includes(e.target.value)) {
        respuesta.push(e.target.value)
      }
    } else {
      respuesta = respuesta.filter(inciso => (inciso !== e.target.value))
    }
    respuestas[this.state.actual].respuesta = respuesta
    this.setState({respuestas: respuestas})
  }

  siguiente() {
    if (false) {
      this.setState({validar: true})
    } else {
      this.setState({actual: this.state.actual + 1})
    }
  }

  anterior() {
    this.setState({actual: this.state.actual - 1})
  }

  enviar() {
    this.props.postResultado({codigo: this.props.codigo, respuestas: this.state.respuestas})
    this.setState({enviado: true})
  }

  generarOpciones(pregunta) {
    switch(pregunta.tipo) {
      case 'nivel':
        return Object.entries(pregunta.opciones).map((opcion, i) => {
          return (
            <Row className='mt-2' key={i}>
              <Col>
                <div className="pretty p-default p-round" style={{fontSize: 18}}>
                  <input type="radio" name={`${pregunta.id}`} value={String.fromCharCode(i + 97)} onChange={this.setRespuestaNoMultiple.bind(this)} />
                  <div className="state p-primary-o">
                      <label>{opcion[1]}</label>
                  </div>
                </div>
              </Col>
            </Row>)
        })
      case 'multiple':
        return Object.entries(pregunta.opciones).map((opcion, i) => {
          return (
            <Row className='mt-2' key={i}>
              <Col>
                <div className="pretty p-default p-curve" style={{fontSize: 18}}>
                  <input type="checkbox" name="color" value={String.fromCharCode(i + 97)} onChange={this.setRespuestaMultiple.bind(this)} />
                  <div className="state p-primary-o">
                      <label>{opcion[1]}</label>
                  </div>
                </div>
              </Col>
            </Row>)
        })
      case 'simple':
        return Object.entries(pregunta.opciones).map((opcion, i) => {
          return (
            <Row className='mt-2' key={i}>
              <Col>
                <div className="pretty p-default p-round" style={{fontSize: 18}}>
                  <input type="radio" name={`${pregunta.id}`} value={String.fromCharCode(i + 97)} onChange={this.setRespuestaNoMultiple.bind(this)} />
                  <div className="state p-primary-o">
                      <label>{opcion[1]}</label>
                  </div>
                </div>
              </Col>
            </Row>)
        })
      default:
        return Object.entries(pregunta.opciones).map((opcion, i) => {
          return (
            <Row className='mt-2' key={i}>
              <Col>
                <div className="pretty p-default p-round" style={{fontSize: 18}}>
                  <input type="radio" name="color" />
                  <div className="state p-primary-o">
                      <label>{opcion[1]}</label>
                  </div>
                </div>
              </Col>
            </Row>)
        })
    }
  }

  render() {
    let pregunta = this.props.preguntas[this.state.actual]
    return (
      <React.Fragment>
        <Row>
          <Col>
            <Steps current={this.state.actual}>
              {this.props.preguntas.map(pregunta => (<Step key={pregunta.indice} />))}
            </Steps>
          </Col>
        </Row>
        <Row>
          <Col>
            <Jumbotron className='mt-2'>
              <Container fluid>
                <Row>
                  <Col className='h4'>
                    {Parser(pregunta.texto)}
                  </Col>
                </Row>
                {
                  pregunta.tipo === 'multiple' && (<Row><Col><small className='text-muted'>Opcion multiple</small></Col></Row>)
                }
                {
                  this.generarOpciones(pregunta)
                }
                <Row className='mt-4'>
                  <Col>
                    {
                      this.state.actual > 0 &&
                      <Button className='mr-2' onClick={this.anterior.bind(this)} color='success'>Anterior</Button>
                    }
                    {
                      (this.props.preguntas.length - 1) === this.state.actual ?
                      (<Button onClick={this.enviar.bind(this)} color='secondary'>Enviar</Button>) :
                      (<Button onClick={this.siguiente.bind(this)} color='success'>Siguiente</Button>)
                    }
                  </Col>
                </Row>
              </Container>
            </Jumbotron>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  codigo: state.codigo,
  preguntas: state.preguntas,
  error: state.error
})

const mapDispatchToProps = dispatch => ({
  postResultado: (resultado) => {dispatch(postResultado(resultado))},
  setCodigo: (codigo) => {dispatch(setCodigo(codigo))},
  clearError: () => {dispatch(clearError())}
})

export default connect(mapStateToProps, mapDispatchToProps)(Preguntas)
