import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, Jumbotron, Label, Input, Button, FormGroup, FormFeedback, FormText } from 'reactstrap'
import { setCodigo, getPreguntas } from "../../../actions"


class Presentacion extends React.PureComponent {

  state = {
    codigo: '',
    validar: false
  }

  componentDidMount() {
    this.props.getPreguntas()
  }

  guardarCodigo() {
    if (this.state.codigo === '' || this.state.codigo.length !== 8 ) {
      this.setState({validar: true})
    } else {
      this.props.setCodigo(this.state.codigo)
      this.props.history.push(`${this.props.match.path}/preguntas`)
    }
  }

  render() {
    return (
      <Row>
        <Col>
          <Jumbotron>
            <div>
              <span className="h1">Bienvenido</span>
            </div>
            <FormGroup>
              <Label for="codigo">Para comenzar ingrese su codigo</Label>
              <Input type='text' id='codigo' placeholder='Codigo' invalid={this.state.validar} onChange={({target}) => {this.setState({codigo: target.value, validar: false})}} />
              <FormFeedback>Debe ingresar un codigo valido</FormFeedback>
              <FormText>Este codigo unico no se almacena con su respuesta.</FormText>
            </FormGroup>
            <Button onClick={this.guardarCodigo.bind(this)} color='success'>Comenzar</Button>
          </Jumbotron>
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
  setCodigo: (codigo) => {dispatch(setCodigo(codigo))},
  getPreguntas: () => {dispatch(getPreguntas())}
})

export default connect(mapStateToProps, mapDispatchToProps)(Presentacion)
