import React from 'react'
import { Row, Col } from 'reactstrap'
import { connect } from 'react-redux'
import { getConfig } from '../../../actions'
import { Redirect } from 'react-router-dom'

const mapStateToProps = (state) => ({config: state.config})
const mapDispatchToProps = (dispatch) => {
  dispatch(getConfig())
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)((props) => {
  return (
    <Row>
      {
        !props.config.activa && (<Redirect to='/en-construccion'/>)
      }
      <Col xs={12}>
        <p className="text-center text-white font-weight-bold h1 mt-2">{props.config.titulo || 'Encuesta'}</p>
      </Col>
    </Row>
  )
})
