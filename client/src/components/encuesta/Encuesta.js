import React from 'react'
import { Container } from 'reactstrap'
import { Switch, Route } from 'react-router-dom'
import Titulo from './titulo/Titulo'
import Presentacion from './presentacion/Presentacion'
import Preguntas from './preguntas/Preguntas'

class Encuesta extends React.PureComponent {
  render() {
    return (
      <Container>
        <Titulo />
        <Switch>
          <Route exact path={this.props.match.url} component={Presentacion} />
          <Route path={`${this.props.match.url}/preguntas`} component={Preguntas} />
        </Switch>
      </Container>
    )
  }
}

export default Encuesta
