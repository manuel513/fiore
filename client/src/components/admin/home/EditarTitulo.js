import React from 'react'
import {Col, Card, CardBody, FormGroup, Input, InputGroup, InputGroupAddon, FormText, Label, Button } from 'reactstrap'

export default (props) => (
  <Col>
    <Card className='pb-0'>
      <CardBody>
        <FormGroup>
          <div className="h4 m-0">
            <Label for="tituloEncuesta">Titulo</Label>
          </div>
          <InputGroup>
            <Input type="text" maxLength={50} name="titulo" id="tituloEncuesta" defaultValue={props.config.titulo} placeholder='Titulo de la encuesta' />
            <InputGroupAddon addonType="append">
              <Button color='success' onClick={() => {
                let entrada = document.getElementById('tituloEncuesta')
                let titulo = entrada.value
                if (titulo === '') {
                  titulo = 'Encuesta'
                }
                if (titulo !== props.config.titulo) {
                  props.changeTitle(titulo)
                }
              }} >
                Guardar
              </Button>
            </InputGroupAddon>
          </InputGroup>
          <FormText>
            Si este campo se encuentra vacio, el titulo ser &quot;Encuesta&quot;
          </FormText>
        </FormGroup>
      </CardBody>
    </Card>
  </Col>
)
