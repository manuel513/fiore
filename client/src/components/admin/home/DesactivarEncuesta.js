import React from 'react'
import { Container, Row, Col, Card, CardBody, Label, FormGroup, Button } from 'reactstrap'
import Switch from 'react-switch';

export default (props) => (
  <Col xs={6}>
    <Card color='danger' outline>
      <CardBody>
        <Container>
          <Row>
            <Col>
              <FormGroup>
                <Label className="h5" for='desactivar'>{props.activa ? 'Desactivar encuesta' : 'Activar Encuesta'}</Label>
                <div>
                  <Switch
                    style={{verticalAlign: 'middle'}}
                    onChange={props.toggle}
                    checked={props.activa}
                    id="desactivar"
                  />
                </div>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup>
                <Label className="h5" for='borrar'>Limpiar encuesta</Label>
                <Button color='danger' id='borrar'>Borrar todo</Button>
              </FormGroup>
            </Col>
          </Row>
        </Container>
      </CardBody>
    </Card>
</Col>)
