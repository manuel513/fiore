import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { getConfig, postConfig, getCodigos, getResultados } from '../../../actions'
import { connect } from 'react-redux'
import Frecuencia from './FrecuenciaRespuesta'
import swal from '@sweetalert/with-react'
import EditarTitulo from './EditarTitulo'
import DesactivarEncuesta from './DesactivarEncuesta'
import Contestados from '../resultados/view/Contestados'

const mapStateToProps = (state) => ({
  codigos: state.codigos,
  resultados: state.resultados,
  config: state.config
})

const mapDispatchToProps = (dispatch) => {
  dispatch(getConfig())
  dispatch(getCodigos())
  dispatch(getResultados())
  return {
    postConfig: (config) => dispatch(postConfig(config))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)((props) => {
  return (
    <Container>
      <Row>
        <EditarTitulo config={props.config} changeTitle={(titulo) => {
          props.postConfig({titulo: titulo})
        }} />
      </Row>
      <Row className='mt-2'>
        <Contestados codigos={props.codigos} resultados={props.resultados} />
        <DesactivarEncuesta activa={props.config.activa} toggle={() => {
          if (props.config.activa) {
            swal('¿Esta seguro de desactivar la encuesta?', 'Cuando la encuesta esta desactivada, se muestra un mensaje de aviso y no se puede contestar la encuesta hasta que sea reactivada', 'warning', {
              buttons: {
                confirm: "Continuar",
                cancel: "Cancelar"
              }
            })
            .then(continuar => {
              if (continuar) {
                props.postConfig({activa: !props.config.activa})
              }
            })
          } else {
                props.postConfig({activa: !props.config.activa})
          }
        }} />
      </Row>
      <Row className='mt-2'>
        <Col>
          <Frecuencia codigos={props.codigos} />
        </Col>
      </Row>
    </Container>
  )
})
