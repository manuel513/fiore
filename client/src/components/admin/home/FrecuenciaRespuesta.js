import React from 'react'
import { LineChart } from 'react-chartkick'
import { Card, CardTitle, CardBody } from 'reactstrap'
import moment from 'moment'
import _ from 'lodash/collection'

class Frecuencia extends React.PureComponent {
  state = {
    fechas: {}
  }

  componentDidMount() {
    this.clasificarFechas()
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.codigos !== this.props.codigos) {
      this.clasificarFechas()
    }
  }

  clasificarFechas() {
    var fechas = this.props.codigos
    .filter(codigo => codigo.usado)
    .map(codigo => codigo.fecha)

    fechas = _.countBy(fechas, fecha => moment(fecha).format('ll'))

    var mago = moment().subtract(30, 'days')

    var marcadas = Object.keys(fechas)

    for (var i = 0; i < 30; i++) {
      let actual = mago.add(1, 'days').format('ll')
      if (marcadas.includes(actual)) {
        continue
      } else {
        fechas[actual] = 0
      }
    }

    this.setState({fechas})
  }

  render() {
    return (
      <Card>
        <CardTitle className='text-center text-secondary h3'>
          Frecuencia de respuestas de los ultimos 30 d&iacute;as
        </CardTitle>
        <CardBody>
          <LineChart curve={false} data={this.state.fechas} messages={{empty: 'No hay datos disponibles'}} />
        </CardBody>
      </Card>
    )
  }
}

export default Frecuencia


