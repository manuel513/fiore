import React, { Component } from 'react'
import { Button, Container, Col, Row, Input, InputGroup, InputGroupAddon, InputGroupText, Form, FormGroup, Label, FormFeedback } from 'reactstrap'
import { patchPregunta, deletePregunta, setError, clearError } from '../../../../actions'
import swal from '@sweetalert/with-react'
import Switch from "react-switch"
import { connect } from 'react-redux'
import ReactQuill from 'react-quill'
import Breadcrumbs from '../../navegacion/Breadcrumbs'
import 'react-quill/dist/quill.snow.css'

class PreguntasEdit extends Component {

  constructor(props){
    super(props)
    this.state = {
        texto: '',
        indice: 0,
        opciones: [],
        tipo: '',
        activa: true,
        invalidar: false,
        incisoActual: '',
        id: ''
    }

    try {
      if (!this.props.match.params.indice) { throw new Error("Pregunta no encontrada") }
      let pIndice = this.props.match.params.indice
      const pregunta = this.props.preguntas.find(
        (pregunta) => {
          /* eslint-disable eqeqeq */
          return (pregunta.indice == pIndice)
          /* eslint-enable eqeqeq */
      })

      if (pregunta === undefined) {throw new Error("Pregunta no encontrada")}

      const { texto, indice, opciones, tipo, activa, id } = pregunta

      this.state = {
        ...this.state,
        opciones: Object.values(opciones),
        texto,
        indice,
        tipo,
        activa,
        id
      }

    } catch (err) {
      this.props.setError({statusCode: 1404, message: (this.props.preguntas.length > 0 ? 'La pregunta solicitada no existe' : 'Ha ocurrido un error, intente de nuevo')})
      this.props.history.push('/admin/preguntas')
    }

    this.handleChangeTexto = this.handleChangeTexto.bind(this)
    this.addNuevoInciso = this.addNuevoInciso.bind(this)
    this.generarOpciones = this.generarOpciones.bind(this)
    this.editInciso = this.editInciso.bind(this)
    this.removeInciso = this.removeInciso.bind(this)
    this.guardar = this.guardar.bind(this)
    this.eliminar = this.eliminar.bind(this)
    this.toggle = this.toggle.bind(this)
  }

  addNuevoInciso(e) {
    if (this.state.incisoActual === '') {
      this.setState({invalidar: true})
      return
    }

    this.setState({
      opciones: [...this.state.opciones, this.state.incisoActual],
      incisoActual: '',
      invalidar: false
    })
  }

  removeInciso(indice) {
    this.setState({opciones: this.state.opciones.filter((opcion, i) => i !== indice)})
  }

  editInciso(valor, indice) {
    let opciones = this.state.opciones
    opciones[indice] = valor
    this.setState({opciones})
  }

  guardar() {
    swal('¡Atencion!', 'Alterar los incisos una vez que se han comenzado a llenar las encuestas puede afectar la precision de los resultados ¿Desea continuar?', 'warning', {
      buttons: {
        confirm: "Continuar",
        cancel: "Cancelar"
      }
    })
    .then((continuar) => {
      if (!continuar) {return}
      const { tipo, texto, opciones, activa, indice } = this.state
      let opcionesObj = opciones.reduce((result, opcion, indice) => {
        result[String.fromCharCode(indice + 97)] = opcion
        return result
      }, {})
      if (texto !== '' && opciones.length > 0) {
        this.props.patchPregunta({ tipo, texto, activa, indice, opciones: opcionesObj }, this.state.id)
        this.props.history.push('/admin/preguntas')
      } else {
        swal('Informacion incompleta', 'Revise los incisos y la pregunta', 'error')
      }
    })
  }

  eliminar()
  {
    swal({
      title: "Esta pregunta sera borrada permanentemente",
      text: "¿desea continuar?",
      icon: "warning",
      buttons: ["Cancelar", "Borrar"],
      dangerMode: true,
    })
    .then(willDelete => {
      if (willDelete) {
        this.props.deletePregunta(this.state.id)
        swal("Eliminado!", "La pregunta ha sido eliminada con exito!", "success")
        .then(() => {
          this.props.history.push('/admin/preguntas')
        })
      }
    })
  }

  toggle() {
    this.setState({ activa: !this.state.activa });
  }

  generarOpciones() {
    let opciones = this.state.opciones.map((opcion, i) => {
      return (
        <Row key={i}>
          <Col>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>{String.fromCharCode(i + 97)}</InputGroupText>
              </InputGroupAddon>
              <Input
                defaultValue={opcion}
                onChange={(e) => {this.editInciso(e.target.value, i)}}
              />
              <InputGroupAddon addonType="append">
                <Button color="danger" onClick={() => {this.removeInciso(i)}}>-</Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </Row>
      );
    });
    opciones.push(
      <Row key={opciones.length}>
        <Col>
          <FormGroup>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  {String.fromCharCode(opciones.length + 97)}
                </InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder="Inciso"
                onChange={
                  (e) => {this.setState({incisoActual: e.target.value, invalidar: false})}
                }
                value={this.state.incisoActual}
                invalid={this.state.invalidar && this.state.incisoActual === ''} />
              <InputGroupAddon addonType="append">
                <Button color="success" onClick={this.addNuevoInciso}>+</Button>
              </InputGroupAddon>
              <FormFeedback>El inciso no puede ir vacio</FormFeedback>
            </InputGroup>
          </FormGroup>
        </Col>
      </Row>
    );
    return opciones;
  }

  handleChangeTexto = (value) => {
    this.setState({texto: value})
  }

  componentWillUnmount() {
    if (!this.state.notfound) {
      this.props.clearError()
    }
  }

  render() {
    return (
      <Container fluid>
        <Row>
          <Col>
            <Breadcrumbs match={this.props.match}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <p className='text-white h2'>
              Editar pregunta #{this.state.indice}
            </p>
          </Col>
        </Row>
        <Row className='mb-2'>
          <Col>
            <Form inline>
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="tipo" className="text-white mr-sm-2">Tipo</Label>
                <Input type='select' value={this.state.tipo} onChange={e => {this.setState({tipo: e.target.value})}} bsSize="sm">
                  <option value="simple">Simple</option>
                  <option value="multiple">Multiple</option>
                  <option value="nivel">Nivel</option>
                </Input>
              </FormGroup>
              <FormGroup className="text-white mb-2 mr-sm-2 mb-sm-0">
                <Label for="indice" className="mr-sm-2">Indice</Label>
                <Input type="number" readOnly value={this.state.indice} name="password" id="indice" bsSize="sm" />
              </FormGroup>
            </Form>
          </Col>
          <Col xs={2}>
            <div className="float-right">
              <Switch
                style={{ verticalAlign: "middle" }}
                onChange={this.toggle}
                checked={this.state.activa}
                uncheckedIcon={false}
                checkedIcon={false}
                id="normal-switch"
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col className='editorcol'>
            <ReactQuill className='bg-light' theme='snow' value={this.state.texto}  onChange={this.handleChangeTexto}/>
          </Col>
        </Row>
        {this.generarOpciones()}
        <Row>
          <Col>
            <Button size='lg' onClick={this.guardar}>Guardar</Button>{' '}
            <Button size='lg' color='danger' onClick={this.eliminar}>Eliminar</Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return { preguntas: state.preguntas }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setError: (error) => {dispatch(setError(error))},
    clearError: () => {dispatch(clearError())},
    patchPregunta: (pregunta, id) => {dispatch(patchPregunta(pregunta, id))},
    deletePregunta: (id) => {dispatch(deletePregunta(id))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PreguntasEdit);
