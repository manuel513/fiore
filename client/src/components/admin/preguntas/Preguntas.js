import React, { Component } from 'react';
import { Container, Col, Row, Button, Alert } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom'
import { clearError, getPreguntas, patchPregunta, swapPregunta, deletePregunta } from '../../../actions'
import View from './view/Pregunta'
import { connect } from 'react-redux'

class Preguntas extends Component {

  constructor(props){
    super(props)
    this.state = {
      alertVisible: (props.error !== null),
      isDragging: false
    }
    this.onDismiss = this.onDismiss.bind(this)
    this.handleDragChange = this.handleDragChange.bind(this)
  }

  handleDragChange (isDragging) {
    this.setState({isDragging})
  }

  componentDidMount() {
    this.props.getPreguntas()
  }

  onDismiss() {
    this.props.clearError()
    this.setState({alertVisible: false})
  }

  componentWillUnmount() {
    if (this.props.error && this.props.error.statusCode !== 401) {
      this.props.clearError()
    }
  }

  render() {
    return (
      <Container fluid>
        <Row>
          <Col>
            {(this.props.error && this.props.error.statusCode === 401) && <Redirect to='/login' />}
            <Alert color='danger' isOpen={this.state.alertVisible} toggle={this.onDismiss}>
              {this.state.alertVisible && (<p>{this.props.error.message}</p>)}
            </Alert>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={{size: 2}}>
            <Button tag={Link} to={`${this.props.match.url}/nueva`} color='info' size='lg'>Nueva pregunta</Button>
          </Col>
        </Row>
        {
          this.state.isDragging &&
          (<Row className='mt-2'>
            <Col>
              <Alert color='warning'>
                <b>¡Atencion!</b> Se recomienda no reacomodar el orden de las preguntas una vez han comenzado a llenar las encuestas
              </Alert>
            </Col>
          </Row>)
        }
        <Row>
        {this.props.preguntas.map((pregunta, i) => (
            <View
              key={i}
              indice={pregunta.indice}
              match={this.props.match}
              isDragging={this.handleDragChange}
              swap={this.props.swapPregunta}
              eliminar={() => {this.props.deletePregunta(pregunta.id)}}
              toggle={() => {this.props.patchPregunta({activa: !pregunta.activa}, pregunta.id)}}
              activo={pregunta.activa}
              texto={pregunta.texto}
              tipo={pregunta.tipo}
              opciones={pregunta.opciones}/>
          ))}
        </Row>
        {
          this.props.preguntas.length < 1 &&
          (
            <Row>
              <Col>
                <p className='text-center display-1 text-white'>No hay preguntas</p>
              </Col>
            </Row>
          )
        }
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return { preguntas: state.preguntas, error: state.error, flag: state.flag }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearError: () => {dispatch(clearError())},
    getPreguntas: () => {dispatch(getPreguntas())},
    patchPregunta: (pregunta, id) => {dispatch(patchPregunta(pregunta, id))},
    swapPregunta: (a, b) => {dispatch(swapPregunta(a, b))},
    deletePregunta: (id) => {dispatch(deletePregunta(id))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Preguntas);
