import React, { Component } from "react";
import {
  Button,
  Container,
  Col,
  Row,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Form,
  FormGroup,
  FormFeedback,
  Label
} from "reactstrap";
import {
  postPregunta,
  setError,
  clearError
} from "../../../../actions";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import swal from '@sweetalert/with-react'
import ReactQuill from "react-quill";
import Switch from "react-switch";
import "react-quill/dist/quill.snow.css";
import Breadcrumbs from '../../navegacion/Breadcrumbs'

class PreguntasCreate extends Component {constructor(props) {
    super(props);
    const { preguntas } = props;
    let ultimo = preguntas
      .sort((a, b) => parseInt(a.indice) - parseInt(b.indice))

    window.ultimo = ultimo
    ultimo = ultimo.findIndex((pregunta, i) => {
      return (i + 1) !== parseInt(pregunta.indice)
    })

    this.state = {
      texto: '',
      opciones: [],
      tipo: 'simple',
      activa: true,
      indice: preguntas.length > 0 ? (ultimo === -1 ? preguntas.length : ultimo) + 1 : 1,
      incisoActual: '',
      invalidar: false
    };

    this.handleChangeTexto = this.handleChangeTexto.bind(this);
    this.addNuevoInciso = this.addNuevoInciso.bind(this);
    this.generarOpciones = this.generarOpciones.bind(this);
    this.editInciso = this.editInciso.bind(this);
    this.removeInciso = this.removeInciso.bind(this);
    this.guardar = this.guardar.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  componentWillUnmount() {
    if (!this.state.notfound) {
      this.props.clearError();
    }
  }

  componentDidUpdate(prevProps){
    if (this.props.preguntas.length > prevProps.preguntas.length) {
      swal('¡Exito!', 'La pregunta ha sido guardada', 'success')
      .then(() => {
        this.props.history.push('/admin/preguntas')
      })
    } else {
      if(this.props.error) {
        swal('¡Error!', 'Ha ocurrido un error, intente de nuevo', 'error')
        this.props.clearError()
      }
    }
  }

  addNuevoInciso(e) {
    if (this.state.incisoActual === '') {
      this.setState({invalidar: true})
      return
    }

    this.setState({
      opciones: [...this.state.opciones, this.state.incisoActual],
      incisoActual: '',
      invalidar: false
    })
  }

  editInciso(valor, indice) {
    let opciones = this.state.opciones
    opciones[indice] = valor
    this.setState({opciones})
  }

  guardar() {
    const { tipo, texto, opciones, activa, indice } = this.state
    let opcionesObj = opciones.reduce((result, opcion, indice) => {
      result[String.fromCharCode(indice + 97)] = opcion
      return result
    }, {})
    console.log(opcionesObj)
    if (texto !== '' && opciones.length > 0) {
      this.props.postPreguntas({ tipo, texto, activa, indice, opciones: opcionesObj })
    } else {
      swal('Informacion incompleta', 'Revise los incisos y la pregunta', 'error')
    }
  }

  removeInciso(indice) {
    this.setState({opciones: this.state.opciones.filter((opcion, i) => i !== indice)})
  }

  generarOpciones() {
    let opciones = this.state.opciones.map((opcion, i) => {
      return (
        <Row key={i}>
          <Col>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>{String.fromCharCode(i + 97)}</InputGroupText>
              </InputGroupAddon>
              <Input
                value={opcion}
                onChange={(e) => {this.editInciso(e.target.value, i)}}
              />
              <InputGroupAddon addonType="append">
                <Button color="danger" onClick={() => {this.removeInciso(i)}}>-</Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </Row>
      );
    });
    opciones.push(
      <Row key={opciones.length}>
        <Col>
          <FormGroup>
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  {String.fromCharCode(opciones.length + 97)}
                </InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder="Inciso"
                onChange={
                  (e) => {this.setState({incisoActual: e.target.value, invalidar: false})}
                }
                value={this.state.incisoActual}
                invalid={this.state.invalidar && this.state.incisoActual === ''} />
              <InputGroupAddon addonType="append">
                <Button color="success" onClick={this.addNuevoInciso}>+</Button>
              </InputGroupAddon>
              <FormFeedback>El inciso no puede ir vacio</FormFeedback>
            </InputGroup>
          </FormGroup>
        </Col>
      </Row>
    );
    return opciones;
  }

  handleChangeTexto = value => {
    this.setState({ texto: value });
  };

  toggle() {
    this.setState({ activa: !this.state.activa });
  }

  render() {
    return (
      <Container fluid>
        <Row>
          <Col>
            <Breadcrumbs match={this.props.match} />
          </Col>
        </Row>
        <Row>
          <Col>
            {this.state.notfound && <Redirect to="/admin/preguntas" />}
            <p className="text-white h2">
              Nueva pregunta
            </p>
          </Col>
        </Row>
        <Row className="mb-2">
          <Col>
            <Form inline>
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="tipo" className="text-white mr-sm-2">
                  Tipo
                </Label>
                <Input type="select" bsSize="sm" onChange={e => {this.setState({tipo: e.target.value})}}>
                  <option value="simple">Simple</option>
                  <option value="multiple">Multiple</option>
                  <option value="nivel">Nivel</option>
                </Input>
              </FormGroup>
              <FormGroup className="text-white mb-2 mr-sm-2 mb-sm-0">
                <Label for="indice" className="mr-sm-2">
                  Indice
                </Label>
                <Input
                  type="number"
                  readOnly
                  value={this.state.indice}
                  name="password"
                  id="indice"
                  bsSize="sm"
                />
              </FormGroup>
            </Form>
          </Col>
          <Col xs={2}>
            <div className="float-right">
              <Switch
                style={{ verticalAlign: "middle" }}
                onChange={this.toggle}
                checked={this.state.activa}
                uncheckedIcon={false}
                checkedIcon={false}
                id="normal-switch"
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col className="editorcol">
            <ReactQuill
              className="bg-light"
              theme="snow"
              value={this.state.texto}
              onChange={this.handleChangeTexto}
            />
          </Col>
        </Row>
        {this.generarOpciones()}
        <Row>
          <Col>
            <Button size='lg' onClick={this.guardar}>Guardar</Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return { preguntas: state.preguntas, error: state.error };
};

const mapDispatchToProps = dispatch => {
  return {
    setError: error => {
      dispatch(setError(error));
    },
    clearError: () => {
      dispatch(clearError());
    },
    postPreguntas: pregunta => {
      dispatch(postPregunta(pregunta));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreguntasCreate);
