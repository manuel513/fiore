import React from 'react'
import Parser from 'html-react-parser';
import swal from '@sweetalert/with-react'
import Switch from 'react-switch';
import { Link } from 'react-router-dom'
import { Container, Row, Col, Card, CardBody, Button, ListGroup, ListGroupItem  } from 'reactstrap';

class PreguntaView extends React.Component {
  constructor(props)
  {
    super(props)

    this.state = {
      isDragged: false
    }

    this.onDragStart = this.onDragStart.bind(this)
    this.onDragEnd = this.onDragEnd.bind(this)
    this.onDragEnter = this.onDragEnter.bind(this)
    this.onDragLeave = this.onDragLeave.bind(this)
    this.onDrop = this.onDrop.bind(this)
  }

  onDragOver(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  onDragEnter(e) {
    e.stopPropagation();
    e.preventDefault();
    if (!this.state.isDragged) {
      e.currentTarget.classList.toggle('border')
      e.currentTarget.classList.toggle('border-danger')
    }
  }

  onDragLeave(e) {
    e.stopPropagation();
    e.preventDefault();
    if (!this.state.isDragged) {
      e.currentTarget.classList.toggle('border')
      e.currentTarget.classList.toggle('border-danger')
    }
  }

  onDragEnd(e) {
    e.stopPropagation();
    this.props.isDragging(false)
    this.setState({isDragged: false})
  }

  onDragStart(e) {
    e.stopPropagation();
    e.dataTransfer.setData('text/plain', this.props.indice)
    this.props.isDragging(true)
    this.setState({isDragged: true})
  }

  onDrop(e) {
    e.currentTarget.classList.toggle('border')
    e.currentTarget.classList.toggle('border-danger')
    if (!this.state.isDragged) {
      let data = parseInt(e.dataTransfer.getData('text'))
      this.props.swap(data, this.props.indice)
    }
  }

  eliminar()
  {
    swal({
      title: "Esta pregunta sera borrada permanentemente",
      text: "¿desea continuar?",
      icon: "warning",
      buttons: ["Cancelar", "Borrar"],
      dangerMode: true,
    })
    .then(willDelete => {
      if (willDelete) {
        this.props.eliminar()
        swal("Eliminado!", "La pregunta ha sido eliminada con exito!", "success");
      }
    })
  }

  render() {
    return (
        <Col xs={12} sm={4} className='mt-2'>
          <Card draggable className={(this.state.isDragged ? 'border border-success' : '')} onDragStart={this.onDragStart} onDragEnd={this.onDragEnd} onDragLeave={this.onDragLeave} onDragEnter={this.onDragEnter} onDragOver={this.onDragOver} onDrop={this.onDrop}>
            <CardBody>
                <Container fluid>
                  <Row>
                    <Col><span className='font-weight-bold'>{`#${this.props.indice}`}</span></Col>
                    <Col>
                      <div className='float-right'>
                        <Switch
                          style={{verticalAlign: 'middle'}}
                          onChange={this.props.toggle}
                          checked={this.props.activo}
                          uncheckedIcon={false}
                          checkedIcon={false}
                          id="normal-switch"
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={{size: 12}}>
                      {Parser(this.props.texto)}
                    </Col>
                  </Row>
                </Container>
              <small className='text-muted text-capitalize'>Tipo: {this.props.tipo}</small>
              <ListGroup>
                {Object.keys(this.props.opciones).map((clave, i) => {
                  return (<ListGroupItem key={i}>
                    <span className='font-weight-bold'>{clave}{': '}</span>{`${this.props.opciones[clave]}`}
                  </ListGroupItem>)
                })}
              </ListGroup>
              <div className='mt-2'>
                <div className='d-inline'>
                  <Button tag={Link} to={`${this.props.match.url}/${this.props.indice}`} outline color='primary'>Editar</Button>{' '}
                </div>
                <div className='d-inline float-right'>
                  <Button outline color='danger' onClick={this.eliminar.bind(this)}>Eliminar</Button>
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
    )
  }

}

export default PreguntaView
