import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom'

const Breadcrumbs = (props) => {
  let direcciones = props.match.url.split('/')
  direcciones.shift()
  let ultimo = direcciones.pop()

  return (
    <div>
      <Breadcrumb className='m-0 p-0'>
        {
          direcciones.map((direccion, i) => {
            if (direccion === 'admin') {
              return (
                <BreadcrumbItem key={i}>
                  <Link to='/admin'>admin</Link>
                </BreadcrumbItem>
              )
            } else {
              return (
                <BreadcrumbItem key={i}>
                  <Link to={`/${direcciones.slice(0, i).join('/')}/${direccion}`}>
                    {direccion}
                  </Link>
                </BreadcrumbItem>
              )
            }
          })
        }
        <BreadcrumbItem active>
          {ultimo}
        </BreadcrumbItem>
      </Breadcrumb>
    </div>
  );
};

export default Breadcrumbs;
