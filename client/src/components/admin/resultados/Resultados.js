import React from 'react'
import { connect } from 'react-redux'
import { Container, Row, Col, Badge, Button, ButtonGroup, UncontrolledTooltip, Card, CardBody } from 'reactstrap'
import { getResultados, getCodigos, getPreguntas, addResultado } from '../../../actions'
import { SERVER_URL } from '../../../constants'
import { FaSync } from 'react-icons/fa'
import downloadCSV from '../../../helpers/export-csv'
import downloadXML from '../../../helpers/export-xml'
import downloadJSON from '../../../helpers/export-json'
import ResultadosView from './view/ResultadosView'
import Contestados from './view/Contestados'


class Resultados extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      conectado: false,
      nuevos: 0
    }

    this.exportarCSV = this.exportarCSV.bind(this)
    this.exportarXML = this.exportarXML.bind(this)
    this.exportarJSON = this.exportarJSON.bind(this)
  }

  horaDeCarga = null

  componentDidMount() {
    this.props.getResultados()
    this.props.getCodigos()
    this.props.getPreguntas()
    this.horaDeCarga = new Date()
    if(typeof(EventSource) !== "undefined") {
      this.source = new EventSource(`${SERVER_URL}/api/resultados/change-stream?_format=event-stream`)

      this.source.onopen = (e) => {
        console.log(e)
        this.setState({conectado: true})
      }

      this.source.addEventListener('data', function(msg) {
        var data = JSON.parse(msg.data);
        console.log(data); // the change object
        this.props.addResultado(data.data)
        this.setState({nuevos: this.state.nuevos + 1})
      }.bind(this))

      this.source.onerror = (err) => {
        console.log(err)
        this.setState({conectado: false})
      }
    } else {
        // Sorry! No server-sent events support..
        console.log('Sorry! No server-sent events support..')
        this.setState({conectado: false})
    }
  }

  exportarCSV() {
    let indices = this.props.preguntas.reduce((result, pregunta) => {
      result[pregunta.id] = pregunta.indice
      return result
    }, {})

    let data = this.props.resultados.map(resultado => {
      return resultado.respuestas.map(respuesta => {
        respuesta['indice'] = indices[respuesta.id]
        delete respuesta['id']
        return respuesta
      })
    })

    downloadCSV({data: data, headers: Object.values(indices).sort(), filename: 'resultados.csv'})
  }

  exportarXML() {
    let indices = this.props.preguntas.reduce((result, pregunta) => {
      result[pregunta.id] = pregunta.indice
      return result
    }, {})

    let data = this.props.resultados.map(resultado => {
      return resultado.respuestas.map(respuesta => {
        respuesta['indice'] = indices[respuesta.id]
        delete respuesta['id']
        delete respuesta['tipo']
        return respuesta
      })
    })

    downloadXML({data: data, filename: 'resultados.xml'})
  }

  exportarJSON() {
    let indices = this.props.preguntas.reduce((result, pregunta) => {
      result[pregunta.id] = pregunta.indice
      return result
    }, {})

    let data = this.props.resultados.map(resultado => {
      return resultado.respuestas.map(respuesta => {
        respuesta['indice'] = indices[respuesta.id]
        delete respuesta['id']
        delete respuesta['tipo']
        return respuesta
      })
    })

    downloadJSON({data: data, filename: 'resultados.json'})
  }

  componentWillUnmount() {
    this.source.onerror = null
    this.source.onopen = null
    this.source.close()
  }

  render(){
    return (
      <Container fluid>
        <Row>
          <Contestados resultados={this.props.resultados} codigos={this.props.codigos} />
          <Col xs={6} lg={4}>
            <Card color='warning' outline id='nuevos'>
              <CardBody>
                <div className="h4 m-0">Nuevos resultados <Badge>{this.state.nuevos}</Badge></div>
                <Row>
                  <Col>
                    <div>Encuestas contestadas desde</div>
                    <div className='text-muted'>{this.horaDeCarga && (`${this.horaDeCarga.toLocaleDateString('es-MX')} ${this.horaDeCarga.toLocaleTimeString('es-MX')}`)}</div>
                  </Col>
                  <Col xs={1}>
                    <div className={'float-right p-1 bg-white rounded border '+ (this.state.conectado ? 'border-success' : 'border-secondary')} id='indicador' >
                      <FaSync className={'font-weight-bold h3 ' + (this.state.conectado ? 'text-success' : 'text-secondary')} />
                      <UncontrolledTooltip placement="top" target="indicador">
                        Actualizacion en tiempo real <span className={(this.state.conectado ? 'text-success' : 'text-danger')}>{(this.state.conectado ? 'conectado' : 'desconectado')}</span>
                      </UncontrolledTooltip>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col xs={false} lg={4} className='text-right'>
            <ButtonGroup size='lg'>
              <div className='btn btn-light dishover'>Exportar</div>
              <Button color='primary' onClick={this.exportarXML}>XML</Button>
              <Button color='primary' onClick={this.exportarCSV}>CSV</Button>
              <Button color='primary' onClick={this.exportarJSON}>JSON</Button>
            </ButtonGroup>
          </Col>
        </Row>
        <Row>
          {this.props.preguntas.map((pregunta, i) => {
              return (
              <ResultadosView key={i} pregunta={pregunta} data={this.props.respuestas.filter(respuesta => respuesta.id === pregunta.id)} />
              )
            })
          }
        </Row>
        {
            (this.props.resultados.length < 1 && this.props.preguntas.length < 1 )&&
            (
              <Row>
                <Col>
                  <p className='text-center display-1 text-white'>No hay resultados</p>
                </Col>
              </Row>
            )
          }
      </Container>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    respuestas: [].concat(...state.resultados.map(resultado => resultado.respuestas)),
    resultados: state.resultados,
    codigos: state.codigos,
    preguntas: state.preguntas
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getResultados: () => { dispatch(getResultados()) },
    getPreguntas: () => { dispatch(getPreguntas()) },
    getCodigos: () => { dispatch(getCodigos()) },
    addResultado: (resultado) => { dispatch(addResultado(resultado)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Resultados)
