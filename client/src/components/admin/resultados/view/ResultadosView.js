import React from 'react'
import Parser from 'html-react-parser';
import { Col, Card, CardBody, CardImg, CardTitle, ListGroup, ListGroupItem, Badge  } from 'reactstrap';
import { LineChart, PieChart, ColumnChart } from 'react-chartkick'
import errorGrafica from './errorgrafica.png'


class ResultadosView extends React.PureComponent {

  chartMessages = {empty: "No hay datos disponibles"}

  state = {
    contador: {},
    recontar: true
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.props.data.length !== prevProps.data.length) && prevState.recontar === false) {
      this.setState({recontar: true})
    }
  }

  // TODO: refactor in componentDidMount using Lodash
  generarGrafica() {
    if (!this.props.data.every(data  => data.tipo === this.props.pregunta.tipo)) {
      console.log("Tipo incorrecto de pregunta ", this.props.pregunta.indice)
    }
    switch(this.props.pregunta.tipo) {
      case 'simple':
        let simples = this.props.data.reduce((result, next) => {
          let respuesta = next.respuesta !== '' ? next.respuesta : 'No Contestado'
          if (result.hasOwnProperty(respuesta)) {
            result[respuesta]++
          } else {
            result[respuesta] = 1
          }
          return result
        }, {})

        if (this.state.recontar) {
          this.setState({contador: simples, recontar: false})
        }
        return (
          <PieChart download={true} messages={this.chartMessages} data={simples} />
        )
      case 'multiple':
        let multiples = this.props.data.reduce((result, next) => {
          if (next.respuesta instanceof Array) {
            if (next.respuesta.length === 0) {
              if (result.hasOwnProperty('No Contestado')) {
                result['No Contestado']++
              } else {
                result['No Contestado'] = 1
              }
            }
            next.respuesta.forEach(respuesta => {
              if (result.hasOwnProperty(respuesta)) {
                result[respuesta]++
              } else {
                result[respuesta] = 1
              }
            })
          }
          return result
        }, {})
        if (this.state.recontar) {
          this.setState({contador: multiples, recontar: false})
        }
        return (
          <ColumnChart download={true} messages={this.chartMessages} data={multiples} />
        )
      case 'nivel':
        let niveles = this.props.data.reduce((result, next) => {
          let respuesta = next.respuesta !== '' ? next.respuesta : 'No Contestado'
          if (result.hasOwnProperty(respuesta)) {
            result[respuesta]++
          } else {
            result[respuesta] = 1
          }
          return result
        }, {})
        if (this.state.recontar) {
          this.setState({contador: niveles, recontar: false})
        }
        return (
          <LineChart download={true} messages={this.chartMessages} data={niveles} />
        )
      default:
        return (
          <CardImg src={errorGrafica} />
        )
    }
  }

  render() {
    return (
        <Col xs={6} lg={4} className='mt-2'>
          <Card>
            <CardBody>
              <CardTitle><span className='text-muted'>{`#${this.props.pregunta.indice} `}</span></CardTitle>
              {this.generarGrafica()}
              <small className='text-muted'>Tipo de pregunta: <strong className='text-capitalize'>{this.props.pregunta.tipo}</strong></small>
              <div className='my-2'>
                {Parser(this.props.pregunta.texto)}
              </div>
              <ListGroup>
                {Object.keys(this.props.pregunta.opciones).map((clave, i) => {
                  return (<ListGroupItem key={i}>
                    <span className='font-weight-bold'>
                      {clave}{': '}
                    </span>
                    {`${this.props.pregunta.opciones[clave]}`}
                    <Badge className='float-right mt-2' pill>
                      {this.state.contador[clave]}
                    </Badge>
                  </ListGroupItem>)
                })}
              </ListGroup>
            </CardBody>
          </Card>
        </Col>
    )
  }

}

export default ResultadosView
