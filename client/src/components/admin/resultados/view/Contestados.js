import React from 'react'
import { Col, UncontrolledTooltip, Card, CardBody, Progress } from 'reactstrap'

export default (props) => (
  <Col xs={6} lg={4}>
    <Card color='success' outline id='resultados'>
      <CardBody>
        <div className="h4 m-0">Encuestas contestadas</div>
        <Progress className='progress-white' color='success' value={(props.resultados.length / props.codigos.length) * 100} />
        <small className="text-muted">{`${props.resultados.length}/${props.codigos.length}`}</small>
        <UncontrolledTooltip target='resultados' placement='top'>
          Cantidad de encuestas contestadas vs. Cantidad de codigos generados
        </UncontrolledTooltip>
      </CardBody>
    </Card>
</Col>)
