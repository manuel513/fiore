import React from 'react';
import { Switch, Route, Link, Redirect } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu';
import { Container, Col, Row } from 'reactstrap';
import { logoutAdmin } from '../../actions';
import { connect } from 'react-redux';
import { FaComment, FaHome, FaPoll, FaCheckSquare, FaSignOutAlt, FaLock } from 'react-icons/fa';
import Home from './home/Home'
import Preguntas from './preguntas/Preguntas';
import Create from './preguntas/create/PreguntaCreate';
import Edit from './preguntas/edit/PreguntaEdit';
import Codigos from './codigos/Codigos';
import Resultados from './resultados/Resultados';

import './Dashboard.css';


const mapStateToProps = (state) => {
  return { error: state.error, token: state.token }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logoutAdmin: () => {dispatch(logoutAdmin())}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)((props) => {
  return (
    <React.Fragment>
      {((props.error && props.error.statusCode === 401) || props.token === null) && (<Redirect to='/login' />)}
      <Menu>
        <p className='h2'>Encuesta</p>
        <Link className='link-sidebar mb-2' to={`${props.match.url}`}><FaHome/>{' '}Inicio</Link>
        <Link className='link-sidebar mb-2' to='/encuesta'><FaPoll/>{' '}Encuesta</Link>
        <Link className='link-sidebar mb-2' to={`${props.match.url}/preguntas`}><FaComment/>{' '}Preguntas</Link>
        <Link className='link-sidebar mb-2' to={`${props.match.url}/resultados`}><FaCheckSquare/>{' '}Resultados</Link>
        <Link className='link-sidebar mb-2' to={`${props.match.url}/codigos`}><FaLock/>{' '}Codigos</Link>
        <Link className='link-sidebar mb-2' to={'/'} onClick={props.logoutAdmin}><FaSignOutAlt/>{' '}Salir</Link>
      </Menu>
      <Container fluid >
        <Row>
          <Col xs={{size: 6, offset: 3}}>
            <p className="text-center text-white font-weight-bold h1 mt-2">Encuesta</p>
          </Col>
        </Row>
        <Row>
          <Col xs={{size: 10, offset: 1}}>
            <Switch>
              <Route exact path={`${props.match.url}`} component={Home} />
              <Route exact path={`${props.match.url}/preguntas`} component={Preguntas}/>
              <Route exact path={`${props.match.url}/preguntas/nueva`} component={Create}/>
              <Route exact path={`${props.match.url}/resultados`} render={(props) => (<Resultados {...props} />)}/>
              <Route path={`${props.match.url}/preguntas/:indice`} component={Edit}/>
              <Route path={`${props.match.url}/codigos/`} component={Codigos}/>
            </Switch>
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  )
})

