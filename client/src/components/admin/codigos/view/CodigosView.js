import React, { Component } from 'react'
import { Row, Col, Table, Pagination, PaginationItem, PaginationLink, InputGroup, InputGroupAddon, InputGroupText, Input, Button, Badge, ButtonGroup, UncontrolledButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledTooltip } from 'reactstrap'
import { FaSearch, FaCheck } from 'react-icons/fa';
import downloadCSV from '../../../../helpers/export-csv'
import downloadJson from '../../../../helpers/export-json'
import Fuse from 'fuse.js'

class CodigoView extends Component {
  state = {
    filtroSeleccionado: 1,
    busqueda: '',
    noResultados: 10,
    pagina: 0
  }

  componentDidMount() {
    let options = {
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: [
        "codigo"
      ]
    }
    this.fuse = new Fuse(this.props.data, options)
  }

  onRadioBtnClick(filtroSeleccionado) {
    this.setState({ filtroSeleccionado, pagina: 0 });
  }

  generarFecha(fecha) {
    if (fecha) {
      let sfecha = new Date(fecha).toLocaleDateString('es-MX')
      let stiempo = new Date(fecha).toLocaleTimeString('es-MX')
      return `${sfecha} ${stiempo}`
    } else {
      return ''
    }
  }

  cambiarPagina(e) {
      e.preventDefault()
      if(e.target.dataset.pagina !== this.state.pagina) {
        this.setState({pagina: e.target.dataset.pagina})
      }
  }

  generarPaginador(codigos) {
    let paginas = codigos.length >= this.state.noResultados ? Math.ceil(codigos.length / this.state.noResultados) : 1
    let links = []

    links.push((
      <PaginationItem key={'<'}>
        <PaginationLink previous href="#" onClick={(e) => {
          e.preventDefault()
          this.setState({pagina: this.state.pagina > 0 ? this.state.pagina - 1 : 0})}
        } />
      </PaginationItem>))

    for (var i = 1; i <= paginas ; i++) {
      let link = (
        <PaginationItem key={i}>
          <PaginationLink href="#" data-pagina={i-1} onClick={this.cambiarPagina.bind(this)}>
            {i}
          </PaginationLink>
        </PaginationItem>)
      links.push(link)
    }

    links.push((
      <PaginationItem key={'>'}>
        <PaginationLink next href="#" onClick={(e) => {
          e.preventDefault()
          this.setState({pagina: this.state.pagina < (paginas - 1) ? this.state.pagina + 1 : paginas - 1})}
        } />
      </PaginationItem>))

    return links
  }

  generarTabla(codigos) {
    let data = codigos.slice((this.state.pagina * 10), (this.state.pagina * 10) +
    (this.state.noResultados))

    return data.map((codigo, i) => {
      return (
        <tr key={i}>
            <th scope="row">{i + (this.state.pagina * 10) + 1}</th>
            <td>{codigo.codigo}</td>
            <td>{codigo.usado ? (<FaCheck className='text-primary' />) : ''}</td>
            <td>{this.generarFecha(codigo.fecha)}</td>
        </tr>
      )
    })
  }

  render() {
    let codigos = this.state.busqueda === '' ? this.props.data : this.fuse.search(this.state.busqueda)
    switch(this.state.filtroSeleccionado) {
      case 1:
        break
      case 2:
        codigos = codigos.filter(codigo => codigo.usado)
        break
      case 3:
        codigos = codigos.filter(codigo => !codigo.usado)
        break
      default:
        break
    }

    return (
      <React.Fragment>
        <Row className='mt-2'>
          <Col xs={4} >
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <FaSearch />
                </InputGroupText>
              </InputGroupAddon>
              <Input placeholder="Codigo" onChange={(e) => {this.setState({busqueda: e.target.value})}} />
            </InputGroup>
          </Col>
          <Col>
            <Input id='noResultados' value={this.state.noResultados} onChange={(e) => {this.setState({noResultados: parseInt(e.target.value)})}} min={10} step={10} type='number'></Input>
            <UncontrolledTooltip placement="top" target="noResultados">
              Numero de resultados listados por pagina
            </UncontrolledTooltip>
          </Col>
          <Col>
            <ButtonGroup>
              <Button color="info" onClick={() => this.onRadioBtnClick(1)} active={this.state.filtroSeleccionado === 1}>
                Todos <Badge pill color='light'>{this.props.data.length}</Badge>
              </Button>
              <Button color="info" onClick={() => this.onRadioBtnClick(2)} active={this.state.filtroSeleccionado === 2}>
                Usados <Badge pill color='light'>{codigos.filter(codigo => codigo.usado).length}</Badge>
              </Button>
              <Button color="info" onClick={() => this.onRadioBtnClick(3)} active={this.state.filtroSeleccionado === 3}>
                Sin Usar <Badge pill color='light'>{codigos.filter(codigo => !codigo.usado).length}</Badge>
              </Button>
            </ButtonGroup>
          </Col>
          <Col>
            <UncontrolledButtonDropdown>
              <DropdownToggle color='success' caret>
                Exportar
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem header>CSV</DropdownItem>
                <DropdownItem onClick={() => {
                  downloadCSV({convert: true, data: codigos, filename: 'codigos.csv'})
                }} >Todos</DropdownItem>
                <DropdownItem onClick={() => {
                  downloadCSV({convert: true, data: codigos.filter(codigo => codigo.usado), filename: 'codigos-usados.csv'})
                }} >Usados</DropdownItem>
                <DropdownItem onClick={() => {
                  downloadCSV({convert: true, data: codigos.filter(codigo => !codigo.usado), filename: 'codigos-sin-usar.csv'})
                }} >Sin usar</DropdownItem>
                <DropdownItem divider />
                <DropdownItem header>JSON</DropdownItem>
                <DropdownItem onClick={() => {
                  downloadJson({convert: true, data: codigos, filename: 'codigos.json'})
                }} >Todos</DropdownItem>
                <DropdownItem onClick={() => {
                  downloadJson({convert: true, data: codigos.filter(codigo => codigo.usado), filename: 'codigos-usados.json'})
                }} >Usados</DropdownItem>
                <DropdownItem onClick={() => {
                  downloadJson({convert: true, data: codigos.filter(codigo => !codigo.usado), filename: 'codigos-sin-usar.json'})
                }} >Sin usar</DropdownItem>
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table hover className='bg-white mt-1'>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Codigo</th>
                  <th>Utilizado</th>
                  <th>Fecha de uso</th>
                </tr>
              </thead>
              <tbody>
                {this.generarTabla(codigos)}
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row>
          <Col className='mx-auto'>
            <Pagination aria-label="Page navigation example">
              {this.generarPaginador(codigos)}
            </Pagination>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

export default CodigoView
