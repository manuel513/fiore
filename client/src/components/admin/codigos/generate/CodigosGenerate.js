import React from 'react'
import { Button, Input } from 'reactstrap'
import swal from '@sweetalert/with-react'

class CodigosGenerate extends React.Component {
  state = {
    cantidad: 1,
    generados: false
  }

  componentDidUpdate(prevProps) {
    if (this.state.generados) {
      if (prevProps.codigos.length <= this.props.codigos.length) {
          swal("¡Exito!", `Se generaron ${this.state.cantidad} codigo(s) nuevo(s).`, 'success')
      } else {
          swal("¡Error!", `No se generaron los codigos solicitados`, 'error')
      }
      this.setState({generados: false})
    }
  }

  obtenerCantidad() {
    swal({
      text: 'Ingrese la cantidad de codigos que desea generar',
      buttons: {
        cancel: {
          text:'Cancelar',
          value: false,
          visible: true,
          closeModal: true
        },
        confirm: {
          text:'Generar',
          value: true,
          closeModal: true
        }
      },
      content: (
        <Input type='number' min={1} onChange={(e) => {this.setState({cantidad: e.target.value})}} />
      )
    })
    .then(confirmado => {
      if (confirmado) {
        this.props.generar(this.state.cantidad)
        this.setState({generados: true})
      }
    })
  }

  render() {
    return (
      <Button onClick={this.obtenerCantidad.bind(this)} color='info' size='lg'>Generar codigos</Button>
    )
  }
}

export default CodigosGenerate
