import React from 'react'
import { connect } from 'react-redux'
import { Container, Row, Col } from 'reactstrap'
import { getCodigos, postCodigos } from '../../../actions'
import Generate from './generate/CodigosGenerate'
import View from './view/CodigosView'

const mapStateToProps = (state) => {
  return { codigos: state.codigos }
}

const mapDispatchToProps = (dispatch) => {
  dispatch(getCodigos())
  return {
    postCodigos: (cantidad) => { dispatch(postCodigos(cantidad)) },
    getCodigos: () => { dispatch(getCodigos()) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)((props) => {
  return (
    <Container fluid>
      <Row>
        <Col xs={12} sm={{size: 2}}>
          <Generate codigos={props.codigos} generar={(cantidad) => {props.postCodigos(cantidad)}} />
        </Col>
      </Row>
      { props.codigos.length > 0 && (<View data={props.codigos} />) }
      {
          props.codigos.length < 1 &&
          (
            <Row>
              <Col>
                <p className='text-center display-1 text-white'>No hay codigos generados</p>
              </Col>
            </Row>
          )
        }
    </Container>
  )
})
