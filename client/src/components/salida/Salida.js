import React from 'react'
import { Container, Row, Col, Jumbotron } from 'reactstrap'
import { FaThumbsUp } from 'react-icons/fa'

export default (props) => {
  return (
    <Container>
      <Row>
        <Col>
          <Jumbotron>
            <div className='text-center'>
              <FaThumbsUp className='Big-logo'/>
            </div>
            <div className='mt-4 text-center'>
              <span className='text-weight-bold display-3'>Muchas gracias</span>
              <p className='h2'>Agradecemos su participacion en esta encuesta</p>
            </div>
          </Jumbotron>
        </Col>
      </Row>
    </Container>
  )
}
