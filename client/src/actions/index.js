import {
  GET_PREGUNTAS,
  GET_CODIGOS,
  GET_RESULTADOS,
  GET_CONFIG,
  LOGIN_ADMIN,
  LOGOUT_ADMIN,
  POST_PREGUNTA,
  POST_CODIGO,
  POST_RESULTADO,
  SET_CODIGO,
  ADD_RESULTADO,
  PATCH_PREGUNTA,
  DELETE_PREGUNTA,
  CLEAR_ERROR,
  SET_ERROR,
  SET,
  CLEAR,
  SERVER_URL
} from "../constants";
import { store } from "../store";
import axios from "axios";

// Config
axios.interceptors.request.use(
  function(config) {
    let state = store.getState();
    let token = state.token || state.token;

    if (token) {
      config.headers["Authorization"] = token;
    }

    return config;
  },
  function(error) {
    // Do something with request error
    console.log("Axios Config Error");
    return Promise.reject(error);
  }
);

// Manejo de errores
export const setError = error => ({ type: SET_ERROR, payload: error });
export const clearError = () => ({ type: CLEAR_ERROR });

const mensajes = {
  0: "Error de conexion",
  400: "Ha ocurrido un error. Intente mas tarde",
  401: "El usuario y/o contraseña no es valido",
  403: "No tiene los permisos para realizar esta accion"
};

const handleError = (error, dispatch) => {
  console.log(error)
  let statusCode = error.response.status || 0;
  dispatch(
    setError({
      statusCode: statusCode,
      message: mensajes[statusCode],
      logout: statusCode === 401
    })
  );
};

// Flag actions
export const set = flag => ({ action: SET, flag });
export const clear = () => ({ action: CLEAR, flag: false });

// GET actions

export const getPreguntas = () => {
  return dispatch => {
    return axios
      .get(`${SERVER_URL}/api/preguntas`)
      .then(response => {
        dispatch(getPreguntasSuccess(response.data));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const getPreguntasSuccess = preguntas => ({
  type: GET_PREGUNTAS,
  payload: preguntas
});

export const getCodigos = () => {
  return dispatch => {
    return axios
      .get(`${SERVER_URL}/api/codigos`)
      .then(response => {
        dispatch(getCodigosSuccess(response.data));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const getCodigosSuccess = codigos => ({
  type: GET_CODIGOS,
  payload: codigos
});

export const getResultados = () => {
  return dispatch => {
    return axios
      .get(`${SERVER_URL}/api/resultados`)
      .then(response => {
        dispatch(getResultadosSuccess(response.data));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const getResultadosSuccess = resultados => ({
  type: GET_RESULTADOS,
  payload: resultados
});

export const getConfig = () => {
  return dispatch => {
    return axios
      .get(`${SERVER_URL}/api/configuraciones`)
      .then(response => {
        dispatch(getConfigSuccess(response.data));
      })
      .catch(error => {
        console.log("AQui", error)
        handleError(error, dispatch);
      });
  };
};

export const getConfigSuccess = config => ({
  type: GET_CONFIG,
  payload: config[0]
});

// POST actions
export const postPregunta = pregunta => {
  return dispatch => {
    return axios
      .post(`${SERVER_URL}/api/preguntas`, pregunta)
      .then(response => {
        dispatch(postPreguntaSuccess(response.data));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const postPreguntaSuccess = pregunta => ({
  type: POST_PREGUNTA,
  payload: pregunta
});

export const postResultado = resultado => {
  return dispatch => {
    return axios
      .post(`${SERVER_URL}/api/resultados`, resultado)
      .then(response => {
        dispatch(postResultadoSuccess(response.data));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const postResultadoSuccess = resultado => ({
  type: POST_RESULTADO,
  payload: resultado
});

export const postCodigos = cantidad => {
  return dispatch => {
    return axios
      .get(`${SERVER_URL}/api/codigos/generate`, {
        params: {
          quantity: cantidad
        }
      })
      .then(response => {
        dispatch(postCodigosSuccess(response.data));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const postCodigosSuccess = codigos => ({
  type: POST_CODIGO,
  payload: codigos
});

export const postConfig = config => {
  return dispatch => {
    return axios
      .post(`${SERVER_URL}/api/configuraciones/update?where=%7B%22monocodigo%22%3A%201%7D`, config)
      .then(response => {
        dispatch(getConfig());
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

// PATCH actions
export const patchPregunta = (pregunta, id) => {
  return dispatch => {
    return axios
      .patch(`${SERVER_URL}/api/preguntas/${id}`, pregunta)
      .then(response => {
        dispatch(patchPreguntaSuccess(response.data));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const patchPreguntaSuccess = pregunta => ({
  type: PATCH_PREGUNTA,
  payload: pregunta
});

export const swapPregunta = (a, b) => {
  return dispatch => {
    return axios
      .post(`${SERVER_URL}/api/preguntas/swap`, { a, b })
      .then(response => {
        if (response.data) {
          dispatch(getPreguntas());
        }
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const deletePregunta = id => {
  return dispatch => {
    return axios
      .delete(`${SERVER_URL}/api/preguntas/${id}`)
      .then(response => {
        dispatch(deletePreguntaSuccess(id));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const deletePreguntaSuccess = id => ({
  type: DELETE_PREGUNTA,
  payload: id
});

// Login and Logout actions
export const loginAdmin = (username, password) => {
  return dispatch => {
    return axios
      .post(`${SERVER_URL}/api/Users/login`, {
        username: username,
        password: password
      })
      .then(response => {
        dispatch(loginAdminSuccess(response.data.id));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const loginAdminSuccess = token => ({
  type: LOGIN_ADMIN,
  payload: token
});

export const logoutAdmin = () => {
  return dispatch => {
    return axios
      .post(`${SERVER_URL}/api/Users/logout`)
      .then(response => {
        dispatch(logoutAdminSuccess(response.data));
      })
      .catch(error => {
        handleError(error, dispatch);
      });
  };
};

export const logoutAdminSuccess = () => ({ type: LOGOUT_ADMIN });

// Event Source actions
export const addResultado = resultado => ({type: ADD_RESULTADO, payload: resultado})

// Public actions

export const setCodigo = codigo => ({type: SET_CODIGO, payload: codigo})
