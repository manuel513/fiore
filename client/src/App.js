import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Dashboard from './components/admin/Dashboard';
import Encuesta from './components/encuesta/Encuesta';
import Login from './components/auth/Login';
import Desactivada from './components/desactivada/Desactivada'
import Salida from './components/salida/Salida'
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
            <Route exact path='/'>
              <Redirect to='/encuesta' />
            </Route>
            <Route path='/encuesta' component={Encuesta} />
            <Route exact path='/login' component={Login} />
            <Route path='/admin' component={Dashboard} />
            <Route exact path='/en-construccion' component={Desactivada} />
            <Route exact path='/gracias' component={Salida} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
