function ToCSV(args) {
  var result, ctr, keys, columnDelimiter, lineDelimiter, data, id;

  data = args.data || null;
  if (data == null || !data.length) {
      return null;
  }

  columnDelimiter = args.columnDelimiter || ',';
  lineDelimiter = args.lineDelimiter || '\n';

  keys = Object.keys(data[0]);

  id = keys.indexOf('id')

  if (id > -1) {
   keys.splice(id, 1);
  }

  result = '';
  result += keys.join(columnDelimiter);
  result += lineDelimiter;

  data.forEach(function(item) {
      ctr = 0;
      keys.forEach(function(key) {
          if (ctr > 0) result += columnDelimiter;
          let valor = (key === 'fecha' && item[key]) ? new Date(item[key]).toLocaleString('es-MX') : item[key]
          result += valor || '';
          ctr++;
      });
      result += lineDelimiter;
  });

  return result;
}

function resultadosToCSV(args) {
  var data = args.data.map((resultado) => {
    return resultado.sort((a,b) => {
      return parseInt(a.indice) - parseInt(b.indice)
    })
  })

  var sheaders = args.headers.join(',') + '\n'

  let csv = ''

  csv += sheaders

  data.forEach(resultado => {
    resultado.forEach(respuesta => {
      if (respuesta.respuesta instanceof Array) {
        csv += '"'
        csv += respuesta.respuesta.join(',')
        csv += '"'
      } else {
        csv += respuesta.respuesta
        csv += ','
      }
    })
    csv += '\n'
  })

  return csv
}

export default function downloadCSV(args) {
  var data, filename, link;
  var csv
  if (args.convert) {
    csv = ToCSV({
      data: args.data
    });
  } else {
    csv = resultadosToCSV(args)
  }

  if (csv == null) return;

  filename = args.filename || 'export.csv';

  if (!csv.match(/^data:text\/csv/i)) {
    csv = 'data:text/csv;charset=utf-8,' + csv;
  }
  data = encodeURI(csv);

  link = document.createElement('a');
  link.setAttribute('href', data);
  link.setAttribute('download', filename);
  link.click();
}
