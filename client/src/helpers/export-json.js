export default function downloadJSON(args) {
    var data, filename, link;
    var json = JSON.stringify(args.data)
    if (json == null) return;

    filename = args.filename || 'export.json';

    if (!json.match(/^data:text\/json/i)) {
      json = 'data:text/json;charset=utf-8,' + json;
    }
    data = encodeURI(json);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
  }
