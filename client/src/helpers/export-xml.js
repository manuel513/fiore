var js2xmlparser = require("js2xmlparser");

export default function downloadXml(args) {
  var data, filename, link;
  var xml = js2xmlparser.parse("resultados", args.data)

  if (xml == null) return;

  filename = args.filename || 'export.xml';

  if (!xml.match(/^data:text\/xml/i)) {
    xml = 'data:text/xml;charset=utf-8,' + xml;
  }
  data = encodeURI(xml);

  link = document.createElement('a');
  link.setAttribute('href', data);
  link.setAttribute('download', filename);
  link.click();
}
