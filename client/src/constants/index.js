export const GET_PREGUNTAS = 'GET_PREGUNTAS'
export const GET_CODIGOS = 'GET_CODIGOS'
export const GET_RESULTADOS = 'GET_RESULTADOS'
export const GET_CONFIG = 'GET_CONFIG'

export const LOGIN_ADMIN = 'LOGIN_ADMIN'
export const LOGOUT_ADMIN = 'LOGOUT_ADMIN'

export const POST_PREGUNTA = 'POST_PREGUNTA'
export const POST_CODIGO = 'POST_CODIGO'
export const POST_RESULTADO = 'POST_RESULTADO'
export const POST_CONFIG = 'POST_CONFIG' // UNUSED

export const SET_CODIGO = 'SET_CODIGO'

export const ADD_PREGUNTA = 'ADD_PREGUNTA' // UNUSED
export const ADD_CODIGO = 'ADD_CODIGO' // UNUSED
export const ADD_RESULTADO = 'ADD_RESULTADO'

export const PATCH_PREGUNTA = 'PATCH_PREGUNTA'
export const SWAP_PREGUNTA = 'SWAP_PREGUNTA'

export const DELETE_PREGUNTA = 'DELETE_PREGUNTA'

export const SET = 'SET' // UNUSED
export const CLEAR = 'CLEAR' // UNUSED

export const SET_ERROR = 'SET_ERROR'
export const CLEAR_ERROR = 'CLEAR_ERROR'

export const SERVER_URL = ''
