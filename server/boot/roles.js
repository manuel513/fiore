'use strict';

module.exports = function(app) {
  var User = app.models.User;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;

  User.create([
    {
      username: 'admin',
      email: 'admin@example.com',
      password: 'randompasswd09'},
  ], function(err, users) {
    if (err) throw err;

    // create the admin role
    Role.create({
      name: 'admin',
    }, function(err, role) {
      if (err) throw err;

      // make an admin
      role.principals.create({
        principalType: RoleMapping.USER,
        principalId: users[0].id,
      }, function(err, principal) {
        if (err) {
          throw err;
        }
      });
    });
  });
};
