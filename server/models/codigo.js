'use strict';

module.exports = function(Codigo) {
  Codigo.validatesUniquenessOf('codigo');

  Codigo.generate = (quantity, cb) => {
    let codigos = [];
    for (var i = 0; i < quantity; i++) {
      codigos.push({
        codigo:
        Math.random().toString(36).substring(2, 6) +
        Math.random().toString(36).substring(2, 6),
      });
    }
    Codigo.create(codigos, (err, instances) => {
      if (!err) {
        cb(null, instances);
      }
    });
  };

  Codigo.remoteMethod('generate', {
    accepts: {arg: 'quantity', type: 'number'},
    returns: {arg: 'codigos', type: 'array'},
    http: {verb: 'get', path: '/generate'},
  });
};
