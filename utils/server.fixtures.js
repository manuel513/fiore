'use strict';
const faker = require('faker');
const request = require('request');

const SERVER_URL = 'http://localhost:3000';

var nivel, multiple, simple;

request.post(`${SERVER_URL}/api/Users/login`,
  {form: {username: 'admin', password: 'randompasswd09'}},
  function(err, response, sbody) {
    if (err) { throw err; };
    var counter = 1;

    var body = JSON.parse(sbody);

    console.log('preguntas');

    request.post(`${SERVER_URL}/api/preguntas?access_token=${body.id}`, {form: {
      'indice': counter++,
      'texto':
      `¿<h1>${faker.lorem.sentence()}<b>${faker.lorem.sentence()}</b></h1>?`,
      'opciones': {
        a: 1,
        b: 2,
        c: 3,
      },
      'tipo': 'nivel',
      'activa': true,
    }}, function(err, response, body) {
      if (err) {
        console.log(err.message);
      } else {
        console.log(body);
        nivel = JSON.parse(body).id;
      }
    });

    request.post(`${SERVER_URL}/api/preguntas?access_token=${body.id}`, {form: {
      'indice': counter++,
      'texto':
      `¿<h1>${faker.lorem.sentence()}<b>${faker.lorem.sentence()}</b></h1>?`,
      'opciones': {
        a: faker.lorem.word(),
        b: faker.lorem.word(),
        c: faker.lorem.word(),
      },
      'tipo': 'multiple',
      'activa': true,
    }}, function(err, response, body) {
      if (err) {
        console.log(err.error.message);
      } else {
        console.log(body);
        multiple = JSON.parse(body).id;
      }
    });

    request.post(`${SERVER_URL}/api/preguntas?access_token=${body.id}`, {form: {
      'indice': counter++,
      'texto':
      `¿<h1>${faker.lorem.sentence()}<b>${faker.lorem.sentence()}</b></h1>?`,
      'opciones': {
        a: faker.lorem.word(),
        b: faker.lorem.word(),
        c: faker.lorem.word(),
      },
      'tipo': 'simple',
      'activa': true,
    }}, function(err, response, body) {
      if (err) {
        console.log(err.message);
      } else {
        console.log(body);
        simple = JSON.parse(body).id;
      }
    });

    console.log('codigos');

    var codigos = ['aaa',
      'bbb',
      'ccc',
      'ddd',
      'eee',
      'fff',
      'ggg',
      'hhh',
      'iii',
      'jjj',
      'kkk'];

    setTimeout(() => {
      for (var i = 10; i >= 0; i--) {
        request.post(
        `${SERVER_URL}/api/codigos?access_token=${body.id}`, {form: {
          codigo: codigos[i],
        }}, function(err, respone, body) {
          if (err) {
            console.log(err);
          } else {
            console.log(body);
          }
        });
      }
    }, 10000);

    setTimeout(() => {
      console.log('codigos ', codigos);

      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'a'},
          {id: multiple, tipo: 'multiple', respuesta: ['a', 'b']},
          {id: simple, tipo: 'simple', respuesta: 'a'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });

      console.log('respuestas');

      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'b'},
          {id: multiple, tipo: 'multiple', respuesta: ['b', 'c']},
          {id: simple, tipo: 'simple', respuesta: 'b'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });

      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'c'},
          {id: multiple, tipo: 'multiple', respuesta: ['c', 'a']},
          {id: simple, tipo: 'simple', respuesta: 'c'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });

      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'a'},
          {id: multiple, tipo: 'multiple', respuesta: ['a', 'b']},
          {id: simple, tipo: 'simple', respuesta: 'a'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });

      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'b'},
          {id: multiple, tipo: 'multiple', respuesta: ['b', 'c']},
          {id: simple, tipo: 'simple', respuesta: 'b'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });

      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'c'},
          {id: multiple, tipo: 'multiple', respuesta: ['c', 'a']},
          {id: simple, tipo: 'simple', respuesta: 'c'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });
      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'a'},
          {id: multiple, tipo: 'multiple', respuesta: ['a', 'b']},
          {id: simple, tipo: 'simple', respuesta: 'a'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });

      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'b'},
          {id: multiple, tipo: 'multiple', respuesta: ['b', 'c']},
          {id: simple, tipo: 'simple', respuesta: 'b'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });

      request.post(`${SERVER_URL}/api/resultados`, {form: {
        codigo: codigos.pop(),
        respuestas: [
          {id: nivel, tipo: 'nivel', respuesta: 'c'},
          {id: multiple, tipo: 'multiple', respuesta: ['c', 'a']},
          {id: simple, tipo: 'simple', respuesta: 'c'},
        ],
      }}, function(err, response, body) {
        if (err) {
          console.log(err);
        } else {
          console.log(body);
        }
      });
    }, 25000);
  }
);
