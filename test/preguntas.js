'use strict';

var app = require('../server/server');
var chai = require('chai');
var supertest =  require('supertest');
var request = supertest(app);

var expect = chai.expect;

var token = '';

describe('Pregunta API Tests', function() {
  describe('Obtener pregunta', function() {
    before(() => {
      return new Promise((resolve) =>{
        request.post('/api/Users/login')
        .send({
          username: 'admin',
          password: 'randompasswd09',
        })
        .end(function(err, res) {
          if (err) { console.log('Ya fue'); }
          token = res.body.id;
          resolve();
        });
      });
    });

    it('Debe obtener un arreglo vacio', function(done) {
      request.get('/api/preguntas').end(function(err, res) {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.be.an('array');
        /* eslint-disable */
        expect(res.body).to.be.empty;
        /* eslint-enable */
        done();
      });
    });

    it('Debe rechazar pregunta sin usuario identificado', function(done) {
      request.post('/api/preguntas')
      .send({
        'indice': 0,
        'texto': '¿Esta es una pregunta?',
        'tipo': '',
        'opciones': {
          a: 'opcion',
          b: 'opcion',
          c: 'opcion',
        },
        'activa': true,
      })
      .end(function(err, res) {
        expect(res.statusCode).to.equal(401);
        expect(res.body).to.be.an('object');
        expect(res.body).to.have.own.property('error');
        done();
      });
    });

    it('Debe enviar un error de validacion por tipo', function(done) {
      request.post('/api/preguntas?access_token=' + token)
      .send({
        'indice': 0,
        'texto': '¿Esta es una pregunta?',
        'tipo': '',
        'opciones': {
          a: 'opcion',
          b: 'opcion',
          c: 'opcion',
        },
        'activa': true,
      })
      .end(function(err, res) {
        expect(res.statusCode).to.equal(422);
        expect(res.body).to.be.an('object');
        expect(res.body.error.details.messages).to.have.own.property('tipo');
        done();
      });
    });

    it('Debe enviar un error de validacion por indice', function(done) {
      request.post('/api/preguntas?access_token=' + token)
      .send({
        'indice': 0,
        'texto': '¿Esta es una pregunta?',
        'tipo': '',
        'opciones': {
          a: 'opcion',
          b: 'opcion',
          c: 'opcion',
        },
        'activa': true,
      })
      .end(function(err, res) {
        request.post('/api/preguntas?access_token=' + token)
      .send(res.body)
      .end(function(err, res) {
        expect(res.statusCode).to.equal(422);
        expect(res.body).to.be.an('object');
        expect(res.body.error.details.messages).to.have.own.property('indice');
        done();
      });
      });
    });
  });
});
