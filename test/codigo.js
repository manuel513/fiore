'use strict';

var app = require('../server/server');
var chai = require('chai');
var supertest =  require('supertest');
var request = supertest(app);

var expect = chai.expect;

var token = '';
describe('Codigo API Tests', function() {
  describe('Obtener codigos', function() {
    before(() => {
      return new Promise((resolve) =>{
        request.post('/api/Users/login')
        .send({
          username: 'admin',
          password: 'randompasswd09',
        })
        .end(function(err, res) {
          if (err) { console.log('Ya fue'); }
          token = res.body.id;
          resolve();
        });
      });
    });

    it('Debe obtener un arreglo vacio', function(done) {
      request.get('/api/codigos?access_token=' + token).end(function(err, res) {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.be.an('array');
        /* eslint-disable */
        expect(res.body).to.be.empty;
        /* eslint-enable */
        done();
      });
    });

    it('Debe enviar un error de validacion por tipo', function(done) {
      request.post('/api/codigos?access_token=' + token)
      .send({
        'codigo': 'abcde',
      })
      .end(function(err, res) {
        request.post('/api/codigos?access_token=' + token)
      .send({
        'codigo': res.body.codigo,
      })
      .end(function(err, res) {
        expect(res.statusCode).to.equal(422);
        expect(res.body).to.be.an('object');
        expect(res.body.error.details.messages).to.have.own.property('codigo');
        done();
      });
      });
    });
  });
});
