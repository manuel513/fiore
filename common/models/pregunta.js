'use strict';

var sanitizeHtml = require('sanitize-html');

module.exports = function(Pregunta) {
  var tipos = ['multiple', 'simple', 'nivel'];
  Pregunta.validate('tipo', function(err) {
    if (!tipos.includes(this.tipo)) { err(); }
  }, {message: 'Tipo de pregunta incorrecto'});

  Pregunta.validatesUniquenessOf('indice');

  Pregunta.observe('before save', function sanearHTML(ctx, next) {
    if (ctx.instance) {
      ctx.instance.texto = sanitizeHtml(ctx.instance.texto, {
        allowedTags: ['b', 'i', 'em', 'strong', 'a'],
        allowedAttributes: {
          'a': ['href'],
        },
      });
    };
    next();
  });

  Pregunta.swap = function(a, b, cb) {
    Pregunta.findOne({where: {indice: a}}, function(err, preguntaA) {
      if (err) {
        cb(err, null);
        return;
      }
      Pregunta.findOne({where: {indice: b}}, function(err, preguntaB) {
        if (err) {
          cb(err, null);
          return;
        }
        preguntaA.updateAttribute('indice', -1, function(err, instanceA) {
          if (err) {
            cb(err, null);
            return;
          }
          preguntaB.updateAttribute('indice', a, function(err, instanceB) {
            if (err) {
              cb(err, null);
              return;
            }
            preguntaA.updateAttribute('indice', b, function(err, instanceA) {
              if (err) {
                cb(err, null);
                return;
              }
              cb(null, true);
            });
          });
        });
      });
    });
  };

  Pregunta.remoteMethod('swap', {
    accepts: [{
      arg: 'a',
      type: 'number',
    },
    {
      arg: 'b',
      type: 'number',
    }],
    returns: {arg: 'success', type: 'boolean'},
  });
};

