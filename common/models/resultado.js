'use strict';
var app = require('../../server/server');

module.exports = function(Resultado) {
  Resultado.validateAsync('codigo', function(verr, done) {
    app.models.Codigo.findOne(
    {where: {codigo: this.codigo}},
    function(ferr, instance) {
      if (ferr) { throw ferr; }
      if (!instance) { verr(); }
      if (instance && instance.usado) {
        verr();
      } else if (instance) {
        instance.updateAttributes({
          usado: true,
          fecha: new Date(Date.now()),
        }, (x, y) => {});
      }
      done();
    });
  }, {message: 'El codigo proporcionado no existe o ya ha sido utilizado'});
};
